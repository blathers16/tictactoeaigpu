#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <limits>
#include "stdafx.h"
#include "..\Tic Tac Toe AI CPU\Constants.h"


__global__ void winO(int o, int square, float * d_results)
{
	if (BOARD_0 & o && BOARD_1 & o && BOARD_2 & o)
	{
		atomicAdd(&d_results[square], INSTA_WIN_VALUE);
		return;
	}

	if (BOARD_3 & o && BOARD_4 & o && BOARD_5 & o)
	{
		atomicAdd(&d_results[square], INSTA_WIN_VALUE);
		return;
	}

	if (BOARD_6 & o && BOARD_7 & o && BOARD_8 & o)
	{
		atomicAdd(&d_results[square], INSTA_WIN_VALUE);
		return;
	}

	if (BOARD_0 & o && BOARD_3 & o && BOARD_6 & o)
	{
		atomicAdd(&d_results[square], INSTA_WIN_VALUE);
		return;
	}

	if (BOARD_1 & o && BOARD_4 & o && BOARD_7 & o)
	{
		atomicAdd(&d_results[square], INSTA_WIN_VALUE);
		return;
	}

	if (BOARD_2 & o && BOARD_5 & o && BOARD_8 & o)
	{
		atomicAdd(&d_results[square], INSTA_WIN_VALUE);
		return;
	}

	if (BOARD_0 & o && BOARD_4 & o && BOARD_8 & o)
	{
		atomicAdd(&d_results[square], INSTA_WIN_VALUE);
		return;
	}

	if (BOARD_2 & o && BOARD_4 & o && BOARD_6 & o)
	{
		atomicAdd(&d_results[square], INSTA_WIN_VALUE);
		return;
	}
}

__global__ void blockX(int x, int square, float * d_results)
{
	if (BOARD_0 & x && BOARD_1 & x && BOARD_2 & x)
	{
		atomicAdd(&d_results[square], BLOCK_VALUE);
		return;
	}

	if (BOARD_3 & x && BOARD_4 & x && BOARD_5 & x)
	{
		atomicAdd(&d_results[square], BLOCK_VALUE);
		return;
	}

	if (BOARD_6 & x && BOARD_7 & x && BOARD_8 & x)
	{
		atomicAdd(&d_results[square], BLOCK_VALUE);
		return;
	}

	if (BOARD_0 & x && BOARD_3 & x && BOARD_6 & x)
	{
		atomicAdd(&d_results[square], BLOCK_VALUE);
		return;
	}

	if (BOARD_1 & x && BOARD_4 & x && BOARD_7 & x)
	{
		atomicAdd(&d_results[square], BLOCK_VALUE);
		return;
	}

	if (BOARD_2 & x && BOARD_5 & x && BOARD_8 & x)
	{
		atomicAdd(&d_results[square], BLOCK_VALUE);
		return;
	}

	if (BOARD_0 & x && BOARD_4 & x && BOARD_8 & x)
	{
		atomicAdd(&d_results[square], BLOCK_VALUE);
		return;
	}

	if (BOARD_2 & x && BOARD_4 & x && BOARD_6 & x)
	{
		atomicAdd(&d_results[square], BLOCK_VALUE);
		return;
	}
}

__global__ void runRecursiveAI(int x, int o, int taken, int square, float* d_results, bool isX, int depth)
{
	if (BOARD_0 & x && BOARD_1 & x && BOARD_2 & x)
	{
		atomicAdd(&d_results[square], (LOSS_VALUE / (LOSS_AGE_FACTOR * (float)depth)));
		return;
	}

	if (BOARD_3 & x && BOARD_4 & x && BOARD_5 & x)
	{
		atomicAdd(&d_results[square], (LOSS_VALUE / (LOSS_AGE_FACTOR * (float)depth)));
		return;
	}

	if (BOARD_6 & x && BOARD_7 & x && BOARD_8 & x)
	{
		atomicAdd(&d_results[square], (LOSS_VALUE / (LOSS_AGE_FACTOR * (float)depth)));
		return;
	}

	if (BOARD_0 & x && BOARD_3 & x && BOARD_6 & x)
	{
		atomicAdd(&d_results[square], (LOSS_VALUE / (LOSS_AGE_FACTOR * (float)depth)));
		return;
	}

	if (BOARD_1 & x && BOARD_4 & x && BOARD_7 & x)
	{
		atomicAdd(&d_results[square], (LOSS_VALUE / (LOSS_AGE_FACTOR * (float)depth)));
		return;
	}

	if (BOARD_2 & x && BOARD_5 & x && BOARD_8 & x)
	{
		atomicAdd(&d_results[square], (LOSS_VALUE / (LOSS_AGE_FACTOR * (float)depth)));
		return;
	}

	if (BOARD_0 & x && BOARD_4 & x && BOARD_8 & x)
	{
		atomicAdd(&d_results[square], (LOSS_VALUE / (LOSS_AGE_FACTOR * (float)depth)));
		return;
	}

	if (BOARD_2 & x && BOARD_4 & x && BOARD_6 & x)
	{
		atomicAdd(&d_results[square], (LOSS_VALUE / (LOSS_AGE_FACTOR * (float)depth)));
		return;
	}




	if (BOARD_0 & o && BOARD_1 & o && BOARD_2 & o)
	{
		atomicAdd(&d_results[square], (WIN_VALUE / (WIN_AGE_FACTOR * (float)depth)));
		return;
	}

	if (BOARD_3 & o && BOARD_4 & o && BOARD_5 & o)
	{
		atomicAdd(&d_results[square], (WIN_VALUE / (WIN_AGE_FACTOR * (float)depth)));
		return;
	}

	if (BOARD_6 & o && BOARD_7 & o && BOARD_8 & o)
	{
		atomicAdd(&d_results[square], (WIN_VALUE / (WIN_AGE_FACTOR * (float)depth)));
		return;
	}

	if (BOARD_0 & o && BOARD_3 & o && BOARD_6 & o)
	{
		atomicAdd(&d_results[square], (WIN_VALUE / (WIN_AGE_FACTOR * (float)depth)));
		return;
	}

	if (BOARD_1 & o && BOARD_4 & o && BOARD_7 & o)
	{
		atomicAdd(&d_results[square], (WIN_VALUE / (WIN_AGE_FACTOR * (float)depth)));
		return;
	}

	if (BOARD_2 & o && BOARD_5 & o && BOARD_8 & o)
	{
		atomicAdd(&d_results[square], (WIN_VALUE / (WIN_AGE_FACTOR * (float)depth)));
		return;
	}

	if (BOARD_0 & o && BOARD_4 & o && BOARD_8 & o)
	{
		atomicAdd(&d_results[square], (WIN_VALUE / (WIN_AGE_FACTOR * (float)depth)));
		return;
	}


	if (taken == 511)
	{
		return;
	}
	else if ((1 << threadIdx.x) & taken)
	{
		return;
	}
	else if (isX)
	{
		cudaStream_t s1;
		cudaStreamCreateWithFlags(&s1, cudaStreamNonBlocking);
		runRecursiveAI << <1, 9, 0, s1 >> > ((x | (1 << threadIdx.x)), o, (taken | (1 << threadIdx.x)), square, d_results, !isX, depth + 1);
		cudaStreamDestroy(s1);
		cudaGetLastError();
	}
	else if (!isX)
	{
		cudaStream_t s2;
		cudaStreamCreateWithFlags(&s2, cudaStreamNonBlocking);
		runRecursiveAI << <1, 9, 0, s2 >> > (x, (o | (1 << threadIdx.x)), (taken | (1 << threadIdx.x)), square, d_results, !isX, depth + 1);
		cudaStreamDestroy(s2);
		cudaGetLastError();
	}
}

__global__ void startRecursiveAI(int x, int o, int taken, float * d_results)
{


	if ((1 << threadIdx.x) & taken)
	{
		return;
	}
	else
	{

		d_results[threadIdx.x] = 0.0f;


		cudaStream_t s1;
		cudaStreamCreateWithFlags(&s1, cudaStreamNonBlocking);
		winO << <1, 1, 0, s1 >> > ((o | (1 << threadIdx.x)), threadIdx.x, d_results);
		


		cudaStream_t s2;
		cudaStreamCreateWithFlags(&s2, cudaStreamNonBlocking);
		blockX << <1, 1, 0, s2 >> > ((x | (1 << threadIdx.x)), threadIdx.x, d_results);
		


		cudaStream_t s3;
		cudaStreamCreateWithFlags(&s3, cudaStreamNonBlocking);
		runRecursiveAI << <1, 9, 0, s3 >> > (x, (o | (1 << threadIdx.x)), (taken | (1 << threadIdx.x)), threadIdx.x, d_results, true, 1);
		cudaDeviceSynchronize();
		cudaStreamDestroy(s1);
		cudaStreamDestroy(s2);
		cudaStreamDestroy(s3);
		cudaGetLastError();
	}
}




extern "C"
{
	__declspec(dllexport) int runAI(int board[])
	{
		float * d_results;

		cudaError_t error = cudaMalloc((void **)&d_results, sizeof(float) * 9);
		if (error != cudaSuccess)
		{
			printf("Error! cudamalloc d_results runai: " + error);
		}

		float* h_results;

		h_results = (float*)malloc(sizeof(float) * 9);

		for (int i = 0; i < 9; i++)
		{
			h_results[i] = -9999999999999999.9f;
		}

		cudaDeviceSynchronize();


		error = cudaMemcpy((void *)d_results, (void *)h_results, sizeof(float) * 9, cudaMemcpyHostToDevice);
		if (error != cudaSuccess)
		{
			printf("Error! cudamemcpy h_results runai: " + error);
		}

		int x = 0;
		int o = 0;
		int taken = 0;

		for (int i = 0; i < 9; i++)
		{
			if (board[i] == 1)
			{
				taken |= (1 << i);
				x |= (1 << i);
			}
			if (board[i] == 2)
			{
				taken |= (1 << i);
				o |= (1 << i);
			}
		}

		cudaDeviceSynchronize();

		startRecursiveAI << <1, 9 >> > (x, o, taken, d_results);

		cudaDeviceSynchronize();
		cudaGetLastError();

		error = cudaMemcpy((void *)h_results, (void *)d_results, sizeof(float) * 9, cudaMemcpyDeviceToHost);
		if (error != cudaSuccess)
		{
			printf("Error! cudamemcpy d_results runai: " + error);
		}

		cudaDeviceSynchronize();

		cudaFree(&d_results);

		int max = 0;
		float maxValue = -999999999999999.9f;

		for (int i = 0; i < 9; i++)
		{
			if (h_results[i] > maxValue)
			{
				max = i;
				maxValue = h_results[i];
			}
		}

		free(h_results);

		return max;
	}
}
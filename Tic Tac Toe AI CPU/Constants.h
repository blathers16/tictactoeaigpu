#ifndef _CONSTANTS_H_INCLUDED_
#define _CONSTANTS_H_INCLUDED_

#define LOSS_VALUE -10.0f
#define WIN_VALUE 1.0f
#define LOSS_AGE_FACTOR 2.0f
#define WIN_AGE_FACTOR 1.0f
#define BLOCK_VALUE 99999999.9f
#define INSTA_WIN_VALUE 999999999.9f

#define BOARD_0 1 << 0
#define BOARD_1 1 << 1
#define BOARD_2 1 << 2
#define BOARD_3 1 << 3
#define BOARD_4 1 << 4
#define BOARD_5 1 << 5
#define BOARD_6 1 << 6
#define BOARD_7 1 << 7
#define BOARD_8 1 << 8

#endif


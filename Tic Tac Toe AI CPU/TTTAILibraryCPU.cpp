#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <limits>
#include "stdafx.h"
#include "Constants.h"


float winO(int o)
{
	if (BOARD_0 & o && BOARD_1 & o && BOARD_2 & o)
	{
		return INSTA_WIN_VALUE;
	}

	if (BOARD_3 & o && BOARD_4 & o && BOARD_5 & o)
	{
		return INSTA_WIN_VALUE;
	}

	if (BOARD_6 & o && BOARD_7 & o && BOARD_8 & o)
	{
		return INSTA_WIN_VALUE;
	}

	if (BOARD_0 & o && BOARD_3 & o && BOARD_6 & o)
	{
		return INSTA_WIN_VALUE;
	}

	if (BOARD_1 & o && BOARD_4 & o && BOARD_7 & o)
	{
		return INSTA_WIN_VALUE;
	}

	if (BOARD_2 & o && BOARD_5 & o && BOARD_8 & o)
	{
		return INSTA_WIN_VALUE;
	}

	if (BOARD_0 & o && BOARD_4 & o && BOARD_8 & o)
	{
		return INSTA_WIN_VALUE;
	}

	if (BOARD_2 & o && BOARD_4 & o && BOARD_6 & o)
	{
		return INSTA_WIN_VALUE;
	}

	return 0.0f;
}

float blockX(int x)
{
	if (BOARD_0 & x && BOARD_1 & x && BOARD_2 & x)
	{
		return BLOCK_VALUE;
	}

	if (BOARD_3 & x && BOARD_4 & x && BOARD_5 & x)
	{
		return BLOCK_VALUE;
	}

	if (BOARD_6 & x && BOARD_7 & x && BOARD_8 & x)
	{
		return BLOCK_VALUE;
	}

	if (BOARD_0 & x && BOARD_3 & x && BOARD_6 & x)
	{
		return BLOCK_VALUE;
	}

	if (BOARD_1 & x && BOARD_4 & x && BOARD_7 & x)
	{
		return BLOCK_VALUE;
	}

	if (BOARD_2 & x && BOARD_5 & x && BOARD_8 & x)
	{
		return BLOCK_VALUE;
	}

	if (BOARD_0 & x && BOARD_4 & x && BOARD_8 & x)
	{
		return BLOCK_VALUE;
	}

	if (BOARD_2 & x && BOARD_4 & x && BOARD_6 & x)
	{
		return BLOCK_VALUE;
	}

	return 0.0f;
}

float runRecursiveAI(int x, int o, int taken, bool isX, int depth)
{
	if (BOARD_0 & x && BOARD_1 & x && BOARD_2 & x)
	{
		return (LOSS_VALUE / (LOSS_AGE_FACTOR * (float)depth));
	}

	if (BOARD_3 & x && BOARD_4 & x && BOARD_5 & x)
	{
		return (LOSS_VALUE / (LOSS_AGE_FACTOR * (float)depth));
	}

	if (BOARD_6 & x && BOARD_7 & x && BOARD_8 & x)
	{
		return (LOSS_VALUE / (LOSS_AGE_FACTOR * (float)depth));
	}

	if (BOARD_0 & x && BOARD_3 & x && BOARD_6 & x)
	{
		return (LOSS_VALUE / (LOSS_AGE_FACTOR * (float)depth));
	}

	if (BOARD_1 & x && BOARD_4 & x && BOARD_7 & x)
	{
		return (LOSS_VALUE / (LOSS_AGE_FACTOR * (float)depth));
	}

	if (BOARD_2 & x && BOARD_5 & x && BOARD_8 & x)
	{
		return (LOSS_VALUE / (LOSS_AGE_FACTOR * (float)depth));
	}

	if (BOARD_0 & x && BOARD_4 & x && BOARD_8 & x)
	{
		return (LOSS_VALUE / (LOSS_AGE_FACTOR * (float)depth));
	}

	if (BOARD_2 & x && BOARD_4 & x && BOARD_6 & x)
	{
		return (LOSS_VALUE / (LOSS_AGE_FACTOR * (float)depth));
	}

	if (BOARD_0 == 1 && BOARD_4 == 1 && BOARD_8 == 1)
	{
		return (LOSS_VALUE / (LOSS_AGE_FACTOR * (float)depth));
	}
	if (BOARD_2 == 1 && BOARD_4 == 1 && BOARD_6 == 1)
	{
		return (LOSS_VALUE / (LOSS_AGE_FACTOR * (float)depth));
	}



	if (BOARD_0 & o && BOARD_1 & o && BOARD_2 & o)
	{
		return (WIN_VALUE / (WIN_AGE_FACTOR * (float)depth));;
	}

	if (BOARD_3 & o && BOARD_4 & o && BOARD_5 & o)
	{
		return (WIN_VALUE / (WIN_AGE_FACTOR * (float)depth));;
	}

	if (BOARD_6 & o && BOARD_7 & o && BOARD_8 & o)
	{
		return (WIN_VALUE / (WIN_AGE_FACTOR * (float)depth));;
	}

	if (BOARD_0 & o && BOARD_3 & o && BOARD_6 & o)
	{
		return (WIN_VALUE / (WIN_AGE_FACTOR * (float)depth));;
	}

	if (BOARD_1 & o && BOARD_4 & o && BOARD_7 & o)
	{
		return (WIN_VALUE / (WIN_AGE_FACTOR * (float)depth));;
	}

	if (BOARD_2 & o && BOARD_5 & o && BOARD_8 & o)
	{
		return (WIN_VALUE / (WIN_AGE_FACTOR * (float)depth));;
	}

	if (BOARD_0 & o && BOARD_4 & o && BOARD_8 & o)
	{
		return (WIN_VALUE / (WIN_AGE_FACTOR * (float)depth));;
	}

	float rValue = 0.0f;

	if (taken == 511)
	{
		return 0.0f;
	}
	else if (isX)
	{
		for (int i = 0; i < 9; i++)
		{
			if (!((1 << i) & taken))
			{
				rValue += runRecursiveAI((x | (1 << i)), o, (taken | (1 << i)), !isX, depth + 1);
			}
		}
	}
	else if (!isX)
	{
		for (int i = 0; i < 9; i++)
		{
			if (!((1 << i) & taken))
			{
				rValue += runRecursiveAI(x, (o | (1 << i)), (taken | (1 << i)), !isX, depth + 1);
			}
		}
	}

	return rValue;
}

void startAICPURecursive(int x, int o, int taken, float * h_results)
{
	for (int i = 0; i < 9; i++)
	{
		if (!(taken & (1 << i)))
		{
			h_results[i] = 0;

			
			h_results[i] += blockX((x | (1 << i)));

			h_results[i] += winO((o | (1 << i)));

			h_results[i] += runRecursiveAI(x, (o | (1 << i)), (taken | (1 << i)), true, 1);
		}
	}
}

extern "C"
{
	__declspec(dllexport) int runAICPU(int board[])
	{

		float* h_results;

		h_results = (float*)malloc(sizeof(float) * 9);

		for (int i = 0; i < 9; i++)
		{
			h_results[i] = -9999999999999999.9f;
		}

		int x = 0;
		int o = 0;
		int taken = 0;

		for (int i = 0; i < 9; i++)
		{
			if (board[i] == 1)
			{
				taken |= (1 << i);
				x |= (1 << i);
			}
			if (board[i] == 2)
			{
				taken |= (1 << i);
				o |= (1 << i);
			}
		}


		startAICPURecursive(x, o, taken, h_results);


		int max = 0;
		float maxValue = -999999999999999.9f;

		for(int i = 0; i < 9; i++)
		{
			if (h_results[i] > maxValue)
			{
				max = i;
				maxValue = h_results[i];
			}
		}

		free(h_results);

		return max;
	}
}
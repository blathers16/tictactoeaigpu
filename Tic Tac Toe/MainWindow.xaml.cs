﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Runtime.InteropServices;
using System.Diagnostics;

namespace Tic_Tac_Toe
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        [DllImport("../../../x64/Debug/Tic Tac Toe AI GPU.dll")]
        public static extern int runAI(int[] board);
        [DllImport("../../../x64/Debug/Tic Tac Toe AI CPU.dll")]
        public static extern int runAICPU(int[] board);

        private int[] board;
        private int taken;
        private int p1Score;
        private int p2Score;
        private bool isX;
        private bool freezeGame;

        public MainWindow()
        {
            InitializeComponent();
            p1Score = 0;
            p2Score = 0;
            InitializeGame();
        }

        private void InitializeGame()
        {
            board = new int[9];
            taken = 0;
            isX = true;
            freezeGame = false;

            turnText.Text = "X";

            for (int i = 0; i < 9; i++)
            {
                board[i] = 0;
            }

            BitmapImage bImage1 = new BitmapImage();
            bImage1.BeginInit();
            bImage1.UriSource = new Uri("D:/Users/blath/Pictures/Pics/xOn.jpg");
            bImage1.EndInit();

            image1.Source = bImage1;

            BitmapImage bImage2 = new BitmapImage();
            bImage2.BeginInit();
            bImage2.UriSource = new Uri("D:/Users/blath/Pictures/Pics/oOff.jpg");
            bImage2.EndInit();

            image2.Source = bImage2;

            BitmapImage ibutton0 = new BitmapImage();
            ibutton0.BeginInit();
            ibutton0.UriSource = new Uri("D:/Users/blath/Pictures/Pics/mouse.jpg");
            ibutton0.EndInit();

            button0.Source = ibutton0;

            BitmapImage ibutton1 = new BitmapImage();
            ibutton1.BeginInit();
            ibutton1.UriSource = new Uri("D:/Users/blath/Pictures/Pics/mouse.jpg");
            ibutton1.EndInit();

            button1.Source = ibutton1;

            BitmapImage ibutton2 = new BitmapImage();
            ibutton2.BeginInit();
            ibutton2.UriSource = new Uri("D:/Users/blath/Pictures/Pics/mouse.jpg");
            ibutton2.EndInit();

            button2.Source = ibutton2;

            BitmapImage ibutton3 = new BitmapImage();
            ibutton3.BeginInit();
            ibutton3.UriSource = new Uri("D:/Users/blath/Pictures/Pics/mouse.jpg");
            ibutton3.EndInit();

            button3.Source = ibutton3;

            BitmapImage ibutton4 = new BitmapImage();
            ibutton4.BeginInit();
            ibutton4.UriSource = new Uri("D:/Users/blath/Pictures/Pics/mouse.jpg");
            ibutton4.EndInit();

            button4.Source = ibutton4;

            BitmapImage ibutton5 = new BitmapImage();
            ibutton5.BeginInit();
            ibutton5.UriSource = new Uri("D:/Users/blath/Pictures/Pics/mouse.jpg");
            ibutton5.EndInit();

            button5.Source = ibutton5;

            BitmapImage ibutton6 = new BitmapImage();
            ibutton6.BeginInit();
            ibutton6.UriSource = new Uri("D:/Users/blath/Pictures/Pics/mouse.jpg");
            ibutton6.EndInit();

            button6.Source = ibutton6;

            BitmapImage ibutton7 = new BitmapImage();
            ibutton7.BeginInit();
            ibutton7.UriSource = new Uri("D:/Users/blath/Pictures/Pics/mouse.jpg");
            ibutton7.EndInit();

            button7.Source = ibutton7;

            BitmapImage ibutton8 = new BitmapImage();
            ibutton8.BeginInit();
            ibutton8.UriSource = new Uri("D:/Users/blath/Pictures/Pics/mouse.jpg");
            ibutton8.EndInit();

            button8.Source = ibutton8;
        }

        private void CheckSolution()
        {
            for (int i = 0; i < 3; i++)
            {
                if (board[(i * 3)] == 1 && board[(i * 3) + 1] == 1 && board[(i * 3) + 2] == 1)
                {
                    p1Score += 2;
                    p1ScoreText.Text = p1Score.ToString();

                    freezeGame = true;
                }

                if (board[i] == 1 && board[i + 3] == 1 && board[i + 6] == 1)
                {
                    p1Score += 2;
                    p1ScoreText.Text = p1Score.ToString();

                    freezeGame = true;
                }
            }

            if (board[0] == 1 && board[4] == 1 && board[8] == 1)
            {
                p1Score += 2;
                p1ScoreText.Text = p1Score.ToString();

                freezeGame = true;
            }

            if (board[2] == 1 && board[4] == 1 && board[6] == 1)
            {
                p1Score += 2;
                p1ScoreText.Text = p1Score.ToString();

                freezeGame = true;
            }

            for (int i = 0; i < 3; i++)
            {
                if (board[(i * 3)] == 2 && board[(i * 3) + 1] == 2 && board[(i * 3) + 2] == 2)
                {
                    p2Score += 2;
                    p2ScoreText.Text = p2Score.ToString();

                    freezeGame = true;
                }

                if (board[i] == 2 && board[i + 3] == 2 && board[i + 6] == 2)
                {
                    p2Score += 2;
                    p2ScoreText.Text = p2Score.ToString();

                    freezeGame = true;
                }
            }

            if (board[0] == 2 && board[4] == 2 && board[8] == 2)
            {
                p2Score += 2;
                p2ScoreText.Text = p2Score.ToString();

                freezeGame = true;
            }

            if (board[2] == 2 && board[4] == 2 && board[6] == 2)
            {
                p2Score += 2;
                p2ScoreText.Text = p2Score.ToString();

                freezeGame = true;
            }

            if (taken == 9)
            {
                p1Score++;
                p2Score++;
                p1ScoreText.Text = p1Score.ToString();
                p2ScoreText.Text = p2Score.ToString();

                freezeGame = true;
            }

            isX = !isX;

            if (isX)
            {
                turnText.Text = "X";
            }
            else if (!isX)
            {
                turnText.Text = "O";
            }

            if (!freezeGame && !isX)
                runO();
        }

        public void runO()
        {
            int x = 0;
            int o = 0;
            int full = 0;

            for (int i = 0; i < 9; i++)
            {
                if (board[i] == 1)
                {
                    x |= 1 << i;
                    full |= 1 << i;
                }
                if (board[i] == 2)
                {
                    o |= 1 << i;
                    full |= 1 << i;
                }
            }

            int nextMove = 0;

            if ((bool)radioButtonGPU.IsChecked)
            {
                Stopwatch stopwatch = Stopwatch.StartNew();
                nextMove = runAI(board);
                stopwatch.Stop();
                elapsedTimeText.Text = stopwatch.ElapsedMilliseconds.ToString() + " ms";
            }
            if((bool)radioButtonCPU.IsChecked)
            {
                Stopwatch stopwatch = Stopwatch.StartNew();
                nextMove = runAICPU(board);
                stopwatch.Stop();
                elapsedTimeText.Text = stopwatch.ElapsedMilliseconds.ToString() + " ms";
            }
            maxSquareText.Text = nextMove.ToString();

            if (nextMove == 0)
            {
                if (board[0] == 0 && !freezeGame)
                {
                    if (isX)
                    {
                        BitmapImage bImage1 = new BitmapImage();
                        bImage1.BeginInit();
                        bImage1.UriSource = new Uri("D:/Users/blath/Pictures/Pics/xOff.jpg");
                        bImage1.EndInit();

                        image1.Source = bImage1;

                        BitmapImage bImage2 = new BitmapImage();
                        bImage2.BeginInit();
                        bImage2.UriSource = new Uri("D:/Users/blath/Pictures/Pics/oOn.jpg");
                        bImage2.EndInit();

                        image2.Source = bImage2;

                        BitmapImage bbutton0 = new BitmapImage();
                        bbutton0.BeginInit();
                        bbutton0.UriSource = new Uri("D:/Users/blath/Pictures/Pics/xCaptured.jpg");
                        bbutton0.EndInit();

                        button0.Source = bbutton0;

                        board[0] = 1;
                        taken++;

                    }
                    else if (!isX)
                    {
                        BitmapImage bImage1 = new BitmapImage();
                        bImage1.BeginInit();
                        bImage1.UriSource = new Uri("D:/Users/blath/Pictures/Pics/xOn.jpg");
                        bImage1.EndInit();

                        image1.Source = bImage1;

                        BitmapImage bImage2 = new BitmapImage();
                        bImage2.BeginInit();
                        bImage2.UriSource = new Uri("D:/Users/blath/Pictures/Pics/oOff.jpg");
                        bImage2.EndInit();

                        image2.Source = bImage2;

                        BitmapImage bbutton0 = new BitmapImage();
                        bbutton0.BeginInit();
                        bbutton0.UriSource = new Uri("D:/Users/blath/Pictures/Pics/oCaptured.jpg");
                        bbutton0.EndInit();

                        button0.Source = bbutton0;

                        board[0] = 2;
                        taken++;
                    }
                    CheckSolution();
                }
            }
            if (nextMove == 1)
            {
                if (board[1] == 0 && !freezeGame)
                {
                    if (isX)
                    {
                        BitmapImage bImage1 = new BitmapImage();
                        bImage1.BeginInit();
                        bImage1.UriSource = new Uri("D:/Users/blath/Pictures/Pics/xOff.jpg");
                        bImage1.EndInit();

                        image1.Source = bImage1;

                        BitmapImage bImage2 = new BitmapImage();
                        bImage2.BeginInit();
                        bImage2.UriSource = new Uri("D:/Users/blath/Pictures/Pics/oOn.jpg");
                        bImage2.EndInit();

                        image2.Source = bImage2;

                        BitmapImage bbutton1 = new BitmapImage();
                        bbutton1.BeginInit();
                        bbutton1.UriSource = new Uri("D:/Users/blath/Pictures/Pics/xCaptured.jpg");
                        bbutton1.EndInit();

                        button1.Source = bbutton1;

                        board[1] = 1;
                        taken++;

                    }
                    else if (!isX)
                    {
                        BitmapImage bImage1 = new BitmapImage();
                        bImage1.BeginInit();
                        bImage1.UriSource = new Uri("D:/Users/blath/Pictures/Pics/xOn.jpg");
                        bImage1.EndInit();

                        image1.Source = bImage1;

                        BitmapImage bImage2 = new BitmapImage();
                        bImage2.BeginInit();
                        bImage2.UriSource = new Uri("D:/Users/blath/Pictures/Pics/oOff.jpg");
                        bImage2.EndInit();

                        image2.Source = bImage2;

                        BitmapImage bbutton1 = new BitmapImage();
                        bbutton1.BeginInit();
                        bbutton1.UriSource = new Uri("D:/Users/blath/Pictures/Pics/oCaptured.jpg");
                        bbutton1.EndInit();

                        button1.Source = bbutton1;

                        board[1] = 2;
                        taken++;
                    }
                    CheckSolution();
                }
            }
            if (nextMove == 2)
            {
                if (board[2] == 0 && !freezeGame)
                {
                    if (isX)
                    {
                        BitmapImage bImage1 = new BitmapImage();
                        bImage1.BeginInit();
                        bImage1.UriSource = new Uri("D:/Users/blath/Pictures/Pics/xOff.jpg");
                        bImage1.EndInit();

                        image1.Source = bImage1;

                        BitmapImage bImage2 = new BitmapImage();
                        bImage2.BeginInit();
                        bImage2.UriSource = new Uri("D:/Users/blath/Pictures/Pics/oOn.jpg");
                        bImage2.EndInit();

                        image2.Source = bImage2;

                        BitmapImage bbutton2 = new BitmapImage();
                        bbutton2.BeginInit();
                        bbutton2.UriSource = new Uri("D:/Users/blath/Pictures/Pics/xCaptured.jpg");
                        bbutton2.EndInit();

                        button2.Source = bbutton2;

                        board[2] = 1;
                        taken++;

                    }
                    else if (!isX)
                    {
                        BitmapImage bImage1 = new BitmapImage();
                        bImage1.BeginInit();
                        bImage1.UriSource = new Uri("D:/Users/blath/Pictures/Pics/xOn.jpg");
                        bImage1.EndInit();

                        image1.Source = bImage1;

                        BitmapImage bImage2 = new BitmapImage();
                        bImage2.BeginInit();
                        bImage2.UriSource = new Uri("D:/Users/blath/Pictures/Pics/oOff.jpg");
                        bImage2.EndInit();

                        image2.Source = bImage2;

                        BitmapImage bbutton2 = new BitmapImage();
                        bbutton2.BeginInit();
                        bbutton2.UriSource = new Uri("D:/Users/blath/Pictures/Pics/oCaptured.jpg");
                        bbutton2.EndInit();

                        button2.Source = bbutton2;

                        board[2] = 2;
                        taken++;
                    }
                    CheckSolution();
                }
            }
            if (nextMove == 3)
            {
                if (board[3] == 0 && !freezeGame)
                {
                    if (isX)
                    {
                        BitmapImage bImage1 = new BitmapImage();
                        bImage1.BeginInit();
                        bImage1.UriSource = new Uri("D:/Users/blath/Pictures/Pics/xOff.jpg");
                        bImage1.EndInit();

                        image1.Source = bImage1;

                        BitmapImage bImage2 = new BitmapImage();
                        bImage2.BeginInit();
                        bImage2.UriSource = new Uri("D:/Users/blath/Pictures/Pics/oOn.jpg");
                        bImage2.EndInit();

                        image2.Source = bImage2;

                        BitmapImage bbutton3 = new BitmapImage();
                        bbutton3.BeginInit();
                        bbutton3.UriSource = new Uri("D:/Users/blath/Pictures/Pics/xCaptured.jpg");
                        bbutton3.EndInit();

                        button3.Source = bbutton3;

                        board[3] = 1;
                        taken++;

                    }
                    else if (!isX)
                    {
                        BitmapImage bImage1 = new BitmapImage();
                        bImage1.BeginInit();
                        bImage1.UriSource = new Uri("D:/Users/blath/Pictures/Pics/xOn.jpg");
                        bImage1.EndInit();

                        image1.Source = bImage1;

                        BitmapImage bImage2 = new BitmapImage();
                        bImage2.BeginInit();
                        bImage2.UriSource = new Uri("D:/Users/blath/Pictures/Pics/oOff.jpg");
                        bImage2.EndInit();

                        image2.Source = bImage2;

                        BitmapImage bbutton3 = new BitmapImage();
                        bbutton3.BeginInit();
                        bbutton3.UriSource = new Uri("D:/Users/blath/Pictures/Pics/oCaptured.jpg");
                        bbutton3.EndInit();

                        button3.Source = bbutton3;

                        board[3] = 2;
                        taken++;
                    }
                    CheckSolution();
                }
            }
            if (nextMove == 4)
            {
                if (board[4] == 0 && !freezeGame)
                {
                    if (isX)
                    {
                        BitmapImage bImage1 = new BitmapImage();
                        bImage1.BeginInit();
                        bImage1.UriSource = new Uri("D:/Users/blath/Pictures/Pics/xOff.jpg");
                        bImage1.EndInit();

                        image1.Source = bImage1;

                        BitmapImage bImage2 = new BitmapImage();
                        bImage2.BeginInit();
                        bImage2.UriSource = new Uri("D:/Users/blath/Pictures/Pics/oOn.jpg");
                        bImage2.EndInit();

                        image2.Source = bImage2;

                        BitmapImage bbutton4 = new BitmapImage();
                        bbutton4.BeginInit();
                        bbutton4.UriSource = new Uri("D:/Users/blath/Pictures/Pics/xCaptured.jpg");
                        bbutton4.EndInit();

                        button4.Source = bbutton4;

                        board[4] = 1;
                        taken++;

                    }
                    else if (!isX)
                    {
                        BitmapImage bImage1 = new BitmapImage();
                        bImage1.BeginInit();
                        bImage1.UriSource = new Uri("D:/Users/blath/Pictures/Pics/xOn.jpg");
                        bImage1.EndInit();

                        image1.Source = bImage1;

                        BitmapImage bImage2 = new BitmapImage();
                        bImage2.BeginInit();
                        bImage2.UriSource = new Uri("D:/Users/blath/Pictures/Pics/oOff.jpg");
                        bImage2.EndInit();

                        image2.Source = bImage2;

                        BitmapImage bbutton4 = new BitmapImage();
                        bbutton4.BeginInit();
                        bbutton4.UriSource = new Uri("D:/Users/blath/Pictures/Pics/oCaptured.jpg");
                        bbutton4.EndInit();

                        button4.Source = bbutton4;

                        board[4] = 2;
                        taken++;
                    }
                    CheckSolution();
                }
            }
            if (nextMove == 5)
            {
                if (board[5] == 0 && !freezeGame)
                {
                    if (isX)
                    {
                        BitmapImage bImage1 = new BitmapImage();
                        bImage1.BeginInit();
                        bImage1.UriSource = new Uri("D:/Users/blath/Pictures/Pics/xOff.jpg");
                        bImage1.EndInit();

                        image1.Source = bImage1;

                        BitmapImage bImage2 = new BitmapImage();
                        bImage2.BeginInit();
                        bImage2.UriSource = new Uri("D:/Users/blath/Pictures/Pics/oOn.jpg");
                        bImage2.EndInit();

                        image2.Source = bImage2;

                        BitmapImage bbutton5 = new BitmapImage();
                        bbutton5.BeginInit();
                        bbutton5.UriSource = new Uri("D:/Users/blath/Pictures/Pics/xCaptured.jpg");
                        bbutton5.EndInit();

                        button5.Source = bbutton5;

                        board[5] = 1;
                        taken++;

                    }
                    else if (!isX)
                    {
                        BitmapImage bImage1 = new BitmapImage();
                        bImage1.BeginInit();
                        bImage1.UriSource = new Uri("D:/Users/blath/Pictures/Pics/xOn.jpg");
                        bImage1.EndInit();

                        image1.Source = bImage1;

                        BitmapImage bImage2 = new BitmapImage();
                        bImage2.BeginInit();
                        bImage2.UriSource = new Uri("D:/Users/blath/Pictures/Pics/oOff.jpg");
                        bImage2.EndInit();

                        image2.Source = bImage2;

                        BitmapImage bbutton5 = new BitmapImage();
                        bbutton5.BeginInit();
                        bbutton5.UriSource = new Uri("D:/Users/blath/Pictures/Pics/oCaptured.jpg");
                        bbutton5.EndInit();

                        button5.Source = bbutton5;

                        board[5] = 2;
                        taken++;
                    }
                    CheckSolution();
                }
            }
            if (nextMove == 6)
            {
                if (board[6] == 0 && !freezeGame)
                {
                    if (isX)
                    {
                        BitmapImage bImage1 = new BitmapImage();
                        bImage1.BeginInit();
                        bImage1.UriSource = new Uri("D:/Users/blath/Pictures/Pics/xOff.jpg");
                        bImage1.EndInit();

                        image1.Source = bImage1;

                        BitmapImage bImage2 = new BitmapImage();
                        bImage2.BeginInit();
                        bImage2.UriSource = new Uri("D:/Users/blath/Pictures/Pics/oOn.jpg");
                        bImage2.EndInit();

                        image2.Source = bImage2;

                        BitmapImage bbutton6 = new BitmapImage();
                        bbutton6.BeginInit();
                        bbutton6.UriSource = new Uri("D:/Users/blath/Pictures/Pics/xCaptured.jpg");
                        bbutton6.EndInit();

                        button6.Source = bbutton6;

                        board[6] = 1;
                        taken++;

                    }
                    else if (!isX)
                    {
                        BitmapImage bImage1 = new BitmapImage();
                        bImage1.BeginInit();
                        bImage1.UriSource = new Uri("D:/Users/blath/Pictures/Pics/xOn.jpg");
                        bImage1.EndInit();

                        image1.Source = bImage1;

                        BitmapImage bImage2 = new BitmapImage();
                        bImage2.BeginInit();
                        bImage2.UriSource = new Uri("D:/Users/blath/Pictures/Pics/oOff.jpg");
                        bImage2.EndInit();

                        image2.Source = bImage2;

                        BitmapImage bbutton6 = new BitmapImage();
                        bbutton6.BeginInit();
                        bbutton6.UriSource = new Uri("D:/Users/blath/Pictures/Pics/oCaptured.jpg");
                        bbutton6.EndInit();

                        button6.Source = bbutton6;

                        board[6] = 2;
                        taken++;
                    }
                    CheckSolution();
                }
            }
            if (nextMove == 7)
            {
                if (board[7] == 0 && !freezeGame)
                {
                    if (isX)
                    {
                        BitmapImage bImage1 = new BitmapImage();
                        bImage1.BeginInit();
                        bImage1.UriSource = new Uri("D:/Users/blath/Pictures/Pics/xOff.jpg");
                        bImage1.EndInit();

                        image1.Source = bImage1;

                        BitmapImage bImage2 = new BitmapImage();
                        bImage2.BeginInit();
                        bImage2.UriSource = new Uri("D:/Users/blath/Pictures/Pics/oOn.jpg");
                        bImage2.EndInit();

                        image2.Source = bImage2;

                        BitmapImage bbutton7 = new BitmapImage();
                        bbutton7.BeginInit();
                        bbutton7.UriSource = new Uri("D:/Users/blath/Pictures/Pics/xCaptured.jpg");
                        bbutton7.EndInit();

                        button7.Source = bbutton7;

                        board[7] = 1;
                        taken++;

                    }
                    else if (!isX)
                    {
                        BitmapImage bImage1 = new BitmapImage();
                        bImage1.BeginInit();
                        bImage1.UriSource = new Uri("D:/Users/blath/Pictures/Pics/xOn.jpg");
                        bImage1.EndInit();

                        image1.Source = bImage1;

                        BitmapImage bImage2 = new BitmapImage();
                        bImage2.BeginInit();
                        bImage2.UriSource = new Uri("D:/Users/blath/Pictures/Pics/oOff.jpg");
                        bImage2.EndInit();

                        image2.Source = bImage2;

                        BitmapImage bbutton7 = new BitmapImage();
                        bbutton7.BeginInit();
                        bbutton7.UriSource = new Uri("D:/Users/blath/Pictures/Pics/oCaptured.jpg");
                        bbutton7.EndInit();

                        button7.Source = bbutton7;

                        board[7] = 2;
                        taken++;
                    }
                    CheckSolution();
                }
            }
            if (nextMove == 8)
            {
                if (board[8] == 0 && !freezeGame)
                {
                    if (isX)
                    {
                        BitmapImage bImage1 = new BitmapImage();
                        bImage1.BeginInit();
                        bImage1.UriSource = new Uri("D:/Users/blath/Pictures/Pics/xOff.jpg");
                        bImage1.EndInit();

                        image1.Source = bImage1;

                        BitmapImage bImage2 = new BitmapImage();
                        bImage2.BeginInit();
                        bImage2.UriSource = new Uri("D:/Users/blath/Pictures/Pics/oOn.jpg");
                        bImage2.EndInit();

                        image2.Source = bImage2;

                        BitmapImage bbutton8 = new BitmapImage();
                        bbutton8.BeginInit();
                        bbutton8.UriSource = new Uri("D:/Users/blath/Pictures/Pics/xCaptured.jpg");
                        bbutton8.EndInit();

                        button8.Source = bbutton8;

                        board[8] = 1;
                        taken++;

                    }
                    else if (!isX)
                    {
                        BitmapImage bImage1 = new BitmapImage();
                        bImage1.BeginInit();
                        bImage1.UriSource = new Uri("D:/Users/blath/Pictures/Pics/xOn.jpg");
                        bImage1.EndInit();

                        image1.Source = bImage1;

                        BitmapImage bImage2 = new BitmapImage();
                        bImage2.BeginInit();
                        bImage2.UriSource = new Uri("D:/Users/blath/Pictures/Pics/oOff.jpg");
                        bImage2.EndInit();

                        image2.Source = bImage2;

                        BitmapImage bbutton8 = new BitmapImage();
                        bbutton8.BeginInit();
                        bbutton8.UriSource = new Uri("D:/Users/blath/Pictures/Pics/oCaptured.jpg");
                        bbutton8.EndInit();

                        button8.Source = bbutton8;

                        board[8] = 2;
                        taken++;
                    }
                    CheckSolution();
                }

            }
        }




        private void button0_Click(object sender, RoutedEventArgs e)
        {
            if (board[0] == 0 && !freezeGame)
            {
                if (isX)
                {
                    BitmapImage bImage1 = new BitmapImage();
                    bImage1.BeginInit();
                    bImage1.UriSource = new Uri("D:/Users/blath/Pictures/Pics/xOff.jpg");
                    bImage1.EndInit();

                    image1.Source = bImage1;

                    BitmapImage bImage2 = new BitmapImage();
                    bImage2.BeginInit();
                    bImage2.UriSource = new Uri("D:/Users/blath/Pictures/Pics/oOn.jpg");
                    bImage2.EndInit();

                    image2.Source = bImage2;

                    BitmapImage bbutton0 = new BitmapImage();
                    bbutton0.BeginInit();
                    bbutton0.UriSource = new Uri("D:/Users/blath/Pictures/Pics/xCaptured.jpg");
                    bbutton0.EndInit();

                    button0.Source = bbutton0;

                    board[0] = 1;
                    taken++;

                }
                else if (!isX)
                {
                    BitmapImage bImage1 = new BitmapImage();
                    bImage1.BeginInit();
                    bImage1.UriSource = new Uri("D:/Users/blath/Pictures/Pics/xOn.jpg");
                    bImage1.EndInit();

                    image1.Source = bImage1;

                    BitmapImage bImage2 = new BitmapImage();
                    bImage2.BeginInit();
                    bImage2.UriSource = new Uri("D:/Users/blath/Pictures/Pics/oOff.jpg");
                    bImage2.EndInit();

                    image2.Source = bImage2;

                    BitmapImage bbutton0 = new BitmapImage();
                    bbutton0.BeginInit();
                    bbutton0.UriSource = new Uri("D:/Users/blath/Pictures/Pics/oCaptured.jpg");
                    bbutton0.EndInit();

                    button0.Source = bbutton0;

                    board[0] = 2;
                    taken++;
                }

                CheckSolution();
            }
        }

        private void button1_Click(object sender, MouseButtonEventArgs e)
        {
            if (board[1] == 0 && !freezeGame)
            {
                if (isX)
                {
                    BitmapImage bImage1 = new BitmapImage();
                    bImage1.BeginInit();
                    bImage1.UriSource = new Uri("D:/Users/blath/Pictures/Pics/xOff.jpg");
                    bImage1.EndInit();

                    image1.Source = bImage1;

                    BitmapImage bImage2 = new BitmapImage();
                    bImage2.BeginInit();
                    bImage2.UriSource = new Uri("D:/Users/blath/Pictures/Pics/oOn.jpg");
                    bImage2.EndInit();

                    image2.Source = bImage2;

                    BitmapImage bbutton1 = new BitmapImage();
                    bbutton1.BeginInit();
                    bbutton1.UriSource = new Uri("D:/Users/blath/Pictures/Pics/xCaptured.jpg");
                    bbutton1.EndInit();

                    button1.Source = bbutton1;

                    board[1] = 1;
                    taken++;

                }
                else if (!isX)
                {
                    BitmapImage bImage1 = new BitmapImage();
                    bImage1.BeginInit();
                    bImage1.UriSource = new Uri("D:/Users/blath/Pictures/Pics/xOn.jpg");
                    bImage1.EndInit();

                    image1.Source = bImage1;

                    BitmapImage bImage2 = new BitmapImage();
                    bImage2.BeginInit();
                    bImage2.UriSource = new Uri("D:/Users/blath/Pictures/Pics/oOff.jpg");
                    bImage2.EndInit();

                    image2.Source = bImage2;

                    BitmapImage bbutton1 = new BitmapImage();
                    bbutton1.BeginInit();
                    bbutton1.UriSource = new Uri("D:/Users/blath/Pictures/Pics/oCaptured.jpg");
                    bbutton1.EndInit();

                    button1.Source = bbutton1;

                    board[1] = 2;
                    taken++;
                }

                CheckSolution();
            }
        }

        private void button2_Click(object sender, MouseButtonEventArgs e)
        {
            if (board[2] == 0 && !freezeGame)
            {
                if (isX)
                {
                    BitmapImage bImage1 = new BitmapImage();
                    bImage1.BeginInit();
                    bImage1.UriSource = new Uri("D:/Users/blath/Pictures/Pics/xOff.jpg");
                    bImage1.EndInit();

                    image1.Source = bImage1;

                    BitmapImage bImage2 = new BitmapImage();
                    bImage2.BeginInit();
                    bImage2.UriSource = new Uri("D:/Users/blath/Pictures/Pics/oOn.jpg");
                    bImage2.EndInit();

                    image2.Source = bImage2;

                    BitmapImage bbutton2 = new BitmapImage();
                    bbutton2.BeginInit();
                    bbutton2.UriSource = new Uri("D:/Users/blath/Pictures/Pics/xCaptured.jpg");
                    bbutton2.EndInit();

                    button2.Source = bbutton2;

                    board[2] = 1;
                    taken++;

                }
                else if (!isX)
                {
                    BitmapImage bImage1 = new BitmapImage();
                    bImage1.BeginInit();
                    bImage1.UriSource = new Uri("D:/Users/blath/Pictures/Pics/xOn.jpg");
                    bImage1.EndInit();

                    image1.Source = bImage1;

                    BitmapImage bImage2 = new BitmapImage();
                    bImage2.BeginInit();
                    bImage2.UriSource = new Uri("D:/Users/blath/Pictures/Pics/oOff.jpg");
                    bImage2.EndInit();

                    image2.Source = bImage2;

                    BitmapImage bbutton2 = new BitmapImage();
                    bbutton2.BeginInit();
                    bbutton2.UriSource = new Uri("D:/Users/blath/Pictures/Pics/oCaptured.jpg");
                    bbutton2.EndInit();

                    button2.Source = bbutton2;

                    board[2] = 2;
                    taken++;
                }

                CheckSolution();
            }
        }

        private void button3_Click(object sender, MouseButtonEventArgs e)
        {
            if (board[3] == 0 && !freezeGame)
            {
                if (isX)
                {
                    BitmapImage bImage1 = new BitmapImage();
                    bImage1.BeginInit();
                    bImage1.UriSource = new Uri("D:/Users/blath/Pictures/Pics/xOff.jpg");
                    bImage1.EndInit();

                    image1.Source = bImage1;

                    BitmapImage bImage2 = new BitmapImage();
                    bImage2.BeginInit();
                    bImage2.UriSource = new Uri("D:/Users/blath/Pictures/Pics/oOn.jpg");
                    bImage2.EndInit();

                    image2.Source = bImage2;

                    BitmapImage bbutton3 = new BitmapImage();
                    bbutton3.BeginInit();
                    bbutton3.UriSource = new Uri("D:/Users/blath/Pictures/Pics/xCaptured.jpg");
                    bbutton3.EndInit();

                    button3.Source = bbutton3;

                    board[3] = 1;
                    taken++;

                }
                else if (!isX)
                {
                    BitmapImage bImage1 = new BitmapImage();
                    bImage1.BeginInit();
                    bImage1.UriSource = new Uri("D:/Users/blath/Pictures/Pics/xOn.jpg");
                    bImage1.EndInit();

                    image1.Source = bImage1;

                    BitmapImage bImage2 = new BitmapImage();
                    bImage2.BeginInit();
                    bImage2.UriSource = new Uri("D:/Users/blath/Pictures/Pics/oOff.jpg");
                    bImage2.EndInit();

                    image2.Source = bImage2;

                    BitmapImage bbutton3 = new BitmapImage();
                    bbutton3.BeginInit();
                    bbutton3.UriSource = new Uri("D:/Users/blath/Pictures/Pics/oCaptured.jpg");
                    bbutton3.EndInit();

                    button3.Source = bbutton3;

                    board[3] = 2;
                    taken++;
                }

                CheckSolution();
            }
        }

        private void button4_Click(object sender, MouseButtonEventArgs e)
        {
            if (board[4] == 0 && !freezeGame)
            {
                if (isX)
                {
                    BitmapImage bImage1 = new BitmapImage();
                    bImage1.BeginInit();
                    bImage1.UriSource = new Uri("D:/Users/blath/Pictures/Pics/xOff.jpg");
                    bImage1.EndInit();

                    image1.Source = bImage1;

                    BitmapImage bImage2 = new BitmapImage();
                    bImage2.BeginInit();
                    bImage2.UriSource = new Uri("D:/Users/blath/Pictures/Pics/oOn.jpg");
                    bImage2.EndInit();

                    image2.Source = bImage2;

                    BitmapImage bbutton4 = new BitmapImage();
                    bbutton4.BeginInit();
                    bbutton4.UriSource = new Uri("D:/Users/blath/Pictures/Pics/xCaptured.jpg");
                    bbutton4.EndInit();

                    button4.Source = bbutton4;

                    board[4] = 1;
                    taken++;

                }
                else if (!isX)
                {
                    BitmapImage bImage1 = new BitmapImage();
                    bImage1.BeginInit();
                    bImage1.UriSource = new Uri("D:/Users/blath/Pictures/Pics/xOn.jpg");
                    bImage1.EndInit();

                    image1.Source = bImage1;

                    BitmapImage bImage2 = new BitmapImage();
                    bImage2.BeginInit();
                    bImage2.UriSource = new Uri("D:/Users/blath/Pictures/Pics/oOff.jpg");
                    bImage2.EndInit();

                    image2.Source = bImage2;

                    BitmapImage bbutton4 = new BitmapImage();
                    bbutton4.BeginInit();
                    bbutton4.UriSource = new Uri("D:/Users/blath/Pictures/Pics/oCaptured.jpg");
                    bbutton4.EndInit();

                    button4.Source = bbutton4;

                    board[4] = 2;
                    taken++;
                }

                CheckSolution();
            }
        }

        private void button5_Click(object sender, MouseButtonEventArgs e)
        {
            if (board[5] == 0 && !freezeGame)
            {
                if (isX)
                {
                    BitmapImage bImage1 = new BitmapImage();
                    bImage1.BeginInit();
                    bImage1.UriSource = new Uri("D:/Users/blath/Pictures/Pics/xOff.jpg");
                    bImage1.EndInit();

                    image1.Source = bImage1;

                    BitmapImage bImage2 = new BitmapImage();
                    bImage2.BeginInit();
                    bImage2.UriSource = new Uri("D:/Users/blath/Pictures/Pics/oOn.jpg");
                    bImage2.EndInit();

                    image2.Source = bImage2;

                    BitmapImage bbutton5 = new BitmapImage();
                    bbutton5.BeginInit();
                    bbutton5.UriSource = new Uri("D:/Users/blath/Pictures/Pics/xCaptured.jpg");
                    bbutton5.EndInit();

                    button5.Source = bbutton5;

                    board[5] = 1;
                    taken++;

                }
                else if (!isX)
                {
                    BitmapImage bImage1 = new BitmapImage();
                    bImage1.BeginInit();
                    bImage1.UriSource = new Uri("D:/Users/blath/Pictures/Pics/xOn.jpg");
                    bImage1.EndInit();

                    image1.Source = bImage1;

                    BitmapImage bImage2 = new BitmapImage();
                    bImage2.BeginInit();
                    bImage2.UriSource = new Uri("D:/Users/blath/Pictures/Pics/oOff.jpg");
                    bImage2.EndInit();

                    image2.Source = bImage2;

                    BitmapImage bbutton5 = new BitmapImage();
                    bbutton5.BeginInit();
                    bbutton5.UriSource = new Uri("D:/Users/blath/Pictures/Pics/oCaptured.jpg");
                    bbutton5.EndInit();

                    button5.Source = bbutton5;

                    board[5] = 2;
                    taken++;
                }

                CheckSolution();
            }
        }

        private void button6_Click(object sender, MouseButtonEventArgs e)
        {
            if (board[6] == 0 && !freezeGame)
            {
                if (isX)
                {
                    BitmapImage bImage1 = new BitmapImage();
                    bImage1.BeginInit();
                    bImage1.UriSource = new Uri("D:/Users/blath/Pictures/Pics/xOff.jpg");
                    bImage1.EndInit();

                    image1.Source = bImage1;

                    BitmapImage bImage2 = new BitmapImage();
                    bImage2.BeginInit();
                    bImage2.UriSource = new Uri("D:/Users/blath/Pictures/Pics/oOn.jpg");
                    bImage2.EndInit();

                    image2.Source = bImage2;

                    BitmapImage bbutton6 = new BitmapImage();
                    bbutton6.BeginInit();
                    bbutton6.UriSource = new Uri("D:/Users/blath/Pictures/Pics/xCaptured.jpg");
                    bbutton6.EndInit();

                    button6.Source = bbutton6;

                    board[6] = 1;
                    taken++;

                }
                else if (!isX)
                {
                    BitmapImage bImage1 = new BitmapImage();
                    bImage1.BeginInit();
                    bImage1.UriSource = new Uri("D:/Users/blath/Pictures/Pics/xOn.jpg");
                    bImage1.EndInit();

                    image1.Source = bImage1;

                    BitmapImage bImage2 = new BitmapImage();
                    bImage2.BeginInit();
                    bImage2.UriSource = new Uri("D:/Users/blath/Pictures/Pics/oOff.jpg");
                    bImage2.EndInit();

                    image2.Source = bImage2;

                    BitmapImage bbutton6 = new BitmapImage();
                    bbutton6.BeginInit();
                    bbutton6.UriSource = new Uri("D:/Users/blath/Pictures/Pics/oCaptured.jpg");
                    bbutton6.EndInit();

                    button6.Source = bbutton6;

                    board[6] = 2;
                    taken++;
                }

                CheckSolution();
            }
        }

        private void button7_Click(object sender, MouseButtonEventArgs e)
        {
            if (board[7] == 0 && !freezeGame)
            {
                if (isX)
                {
                    BitmapImage bImage1 = new BitmapImage();
                    bImage1.BeginInit();
                    bImage1.UriSource = new Uri("D:/Users/blath/Pictures/Pics/xOff.jpg");
                    bImage1.EndInit();

                    image1.Source = bImage1;

                    BitmapImage bImage2 = new BitmapImage();
                    bImage2.BeginInit();
                    bImage2.UriSource = new Uri("D:/Users/blath/Pictures/Pics/oOn.jpg");
                    bImage2.EndInit();

                    image2.Source = bImage2;

                    BitmapImage bbutton7 = new BitmapImage();
                    bbutton7.BeginInit();
                    bbutton7.UriSource = new Uri("D:/Users/blath/Pictures/Pics/xCaptured.jpg");
                    bbutton7.EndInit();

                    button7.Source = bbutton7;

                    board[7] = 1;
                    taken++;

                }
                else if (!isX)
                {
                    BitmapImage bImage1 = new BitmapImage();
                    bImage1.BeginInit();
                    bImage1.UriSource = new Uri("D:/Users/blath/Pictures/Pics/xOn.jpg");
                    bImage1.EndInit();

                    image1.Source = bImage1;

                    BitmapImage bImage2 = new BitmapImage();
                    bImage2.BeginInit();
                    bImage2.UriSource = new Uri("D:/Users/blath/Pictures/Pics/oOff.jpg");
                    bImage2.EndInit();

                    image2.Source = bImage2;

                    BitmapImage bbutton7 = new BitmapImage();
                    bbutton7.BeginInit();
                    bbutton7.UriSource = new Uri("D:/Users/blath/Pictures/Pics/oCaptured.jpg");
                    bbutton7.EndInit();

                    button7.Source = bbutton7;

                    board[7] = 2;
                    taken++;
                }

                CheckSolution();
            }
        }

        private void button8_Click(object sender, MouseButtonEventArgs e)
        {
            if (board[8] == 0 && !freezeGame)
            {
                if (isX)
                {
                    BitmapImage bImage1 = new BitmapImage();
                    bImage1.BeginInit();
                    bImage1.UriSource = new Uri("D:/Users/blath/Pictures/Pics/xOff.jpg");
                    bImage1.EndInit();

                    image1.Source = bImage1;

                    BitmapImage bImage2 = new BitmapImage();
                    bImage2.BeginInit();
                    bImage2.UriSource = new Uri("D:/Users/blath/Pictures/Pics/oOn.jpg");
                    bImage2.EndInit();

                    image2.Source = bImage2;

                    BitmapImage bbutton8 = new BitmapImage();
                    bbutton8.BeginInit();
                    bbutton8.UriSource = new Uri("D:/Users/blath/Pictures/Pics/xCaptured.jpg");
                    bbutton8.EndInit();

                    button8.Source = bbutton8;

                    board[8] = 1;
                    taken++;

                }
                else if (!isX)
                {
                    BitmapImage bImage1 = new BitmapImage();
                    bImage1.BeginInit();
                    bImage1.UriSource = new Uri("D:/Users/blath/Pictures/Pics/xOn.jpg");
                    bImage1.EndInit();

                    image1.Source = bImage1;

                    BitmapImage bImage2 = new BitmapImage();
                    bImage2.BeginInit();
                    bImage2.UriSource = new Uri("D:/Users/blath/Pictures/Pics/oOff.jpg");
                    bImage2.EndInit();

                    image2.Source = bImage2;

                    BitmapImage bbutton8 = new BitmapImage();
                    bbutton8.BeginInit();
                    bbutton8.UriSource = new Uri("D:/Users/blath/Pictures/Pics/oCaptured.jpg");
                    bbutton8.EndInit();

                    button8.Source = bbutton8;

                    board[8] = 2;
                    taken++;
                }

                CheckSolution();
            }
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            InitializeGame();
        }
    }
}
